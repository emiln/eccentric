const { getMainWindow } = require('./main-window.js')

let mode = 'NORMAL';

function pressKey(event, keyCode) {
  const mainWindow = getMainWindow();
  const activeBuffer = mainWindow.getBrowserView();
  const exec = (f) => activeBuffer.webContents.executeJavaScript(`(${f.toString()})()`);
  const key = keyCode.key;

  if (mode === 'NORMAL') {
    event.preventDefault();
    if (key === 'i') {
      mode = 'INSERT';
      mainWindow.webContents.send('set-mode', 'INSERT');
    }
    if (key === 'h') {
      exec(() => {
        window.scrollBy({
          behavior: 'instant',
          left: -32,
          top: 0,
        });
      });
    }
    if (key === 'j') {
      exec(() => {
        window.scrollBy({
          behavior: 'instant',
          left: 0,
          top: 32,
        });
      });
    }
    if (key === 'k') {
      exec(() => {
        window.scrollBy({
          behavior: 'instant',
          left: 0,
          top: -32,
        });
      });
    }
    if (key === 'l') {
      exec(() => {
        window.scrollBy({
          behavior: 'instant',
          left: 32,
          top: 0,
        });
      });
    }
    if (key === 'q') {
      process.exit();
    }
  } else {
    if (key === 'Escape') {
      event.preventDefault();
      mode = 'NORMAL';
      mainWindow.webContents.send('set-mode', 'NORMAL');
    }
  }
}

module.exports = {
  pressKey,
};
