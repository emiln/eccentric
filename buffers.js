const { BrowserView, ipcMain } = require('electron');
const { pressKey } = require('./keybindings.js');

function createBuffer(window, url) {
  const buffer = new BrowserView();
  const { height, width } = window.getBounds();
  buffer.setBounds({
    height: height - 20,
    width: width,
    x: 0,
    y: 0,
  });
  buffer.webContents.loadURL(url);
  buffer.webContents.on('before-input-event', pressKey);
  buffer.webContents.on('did-start-navigation', (event, url, _, inPlace) => {
    if (inPlace) {
      ipcMain.send('navigated-to', url, buffer.id);
    }
  });
  return buffer;
}

function setActiveBuffer(window, buffer) {
  window.setBrowserView(buffer);
  buffer.webContents.once('did-finish-load', () => buffer.webContents.focus());
}

module.exports = {
  createBuffer,
  setActiveBuffer,
};
