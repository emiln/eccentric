const { app, BrowserView, BrowserWindow, ipcMain } = require('electron');
const { 'argv': [,, url] } = process;

const { createBuffer, setActiveBuffer } = require('./buffers.js');
const { pressKey } = require('./keybindings.js');
const { getMainWindow } = require('./main-window.js');

function resize() {
  const mainWindow = getMainWindow();
  for (const buffer of BrowserView.getAllViews()) {
    const {width, height} = mainWindow.getBounds();
    buffer.setBounds({x:0,y:0,width, height: height - 20});
  }
}

function main() {
  const mainWindow = getMainWindow();
  mainWindow.webContents.on('before-input-event', pressKey);
  if (url) {
    setActiveBuffer(mainWindow, createBuffer(mainWindow, url));
  }
  mainWindow.on('resize', resize);
}

ipcMain.on('navigate-to', (event, newUrl) => {
  const activeView = browserWindow.getBrowserView();
  activeView.webContents.loadURL(newUrl);
});

app.on('ready', main);
app.on('window-all-closed', app.quit);
