const { app, BrowserWindow } = require('electron');

let mainWindow = null;
app.on('ready', () => mainWindow = createMainWindow());

function createMainWindow() {
  const browserWindow = new BrowserWindow({
    width: 800,
    height: 600,
    frame: false,
    title: 'eccentric',
    autoHideMenuBar: true,
    enableLargerThanScreen: true,
    backgroundColor: 'white',
  });

  browserWindow.loadFile('modal-bar.html');

  browserWindow.webContents.once('did-finish-load', () => browserWindow.webContents.send('set-mode', 'NORMAL'));

  return browserWindow;
}

module.exports = {
  'getMainWindow': () => mainWindow,
};
